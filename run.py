from flask import Flask, render_template, request
import docker

client = docker.from_env()
cont = client.containers

app = Flask(__name__)

@app.route("/")
def index():
    return "Other text!"


@app.route("/containers")
def containers():

    containers = cont.list()
    return render_template("containers.html", title="Containers list", containers=containers)


@app.route("/logs", methods=["GET"])
def conrainer_logs():
    cid = request.args.get('cid')
    if cid:
        logs = cont.list("since", cid)[0].logs().decode("utf-8")
        return render_template("logs.html", id=cid, logs=logs)
    else:
        return "Bad request!"


if __name__ == "__main__":
    app.run(host="0.0.0.0")

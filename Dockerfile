FROM python:3.7
ADD . /code
WORKDIR /code
RUN apt update && apt install -yy docker.io && service docker start
RUN pip install -r requirements.txt
CMD python run.py
